
const dashboardroutes = [
    {
        name:'Homepage',
        path:'/homepage',
        component: () => import(/* webpackChunkName: "homepage" */ '../views/dashboard/Homepage.vue'),
        meta:{
          layout:'Dashboard',
          authentication:true
        }
    },
        {
        name:'Dashboards',
        path:'/homepage/dashboards',
        component: () => import(/* webpackChunkName: "dashboards" */ '../views/dashboard/Dashboards.vue'),
        meta:{
          layout:'Dashboard',
          authentication:true
        }
    },
    {
      name:'Devices',
      path:'/homepage/devices',
      component: () => import(/* webpackChunkName: "devices" */ '../views/dashboard/Devices.vue'),
      meta:{
        layout:'Dashboard',
        authentication:true
      }
    },
    {
      name:'Account',
      path:'/homepage/me',
      component: () => import(/* webpackChunkName: "Account" */ '../views/account/Account.vue'),
      meta:{
        layout:'Dashboard',
        authentication:true
      }
    },
    {
      name:'DashAbout',
      path:'/homepage/about',
      component: () => import(/* webpackChunkName: "dashabout" */ '../views/dashboard/About.vue'),
      meta:{
        layout:'Dashboard',
        authentication:true
      }
    },
    {
      name:'Desktop',
      path:'/homepage/desktop',
      component: () => import(/* webpackChunkName: "desktop" */ '../views/dashboard/Desktop.vue'),
      meta:{
        layout:'Dashboard',
        authentication:true
      }
    }
]

export default dashboardroutes