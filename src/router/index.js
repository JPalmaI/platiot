import { createRouter, createWebHistory } from 'vue-router'
import defaultroutes from './defaultroutes';
import dashboardroutes from './dashboardroutes';
import { useUserStore } from '@/stores/userstore';

const routes = defaultroutes.concat(dashboardroutes);

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: routes.concat(
        {
            path: '/:pathMatch(.*)*',
            component: () => import(/* webpackChunkName: "NotFound" */ '../views/general/NotFound.vue'),
            meta: {
                layout:'Default',
                authentication: false
            },
        },
    )
})

/* 
 Antes de entrar a cada ruta ejecuta esta funcion
*/
router.beforeEach((to, from, next) => {
    //La store se importa dentro como recomienda pinia
    const store = useUserStore();
    const { isValidSession } = store;
    /* 
        Si voy al login y ya tengo una session valida
    */
    if ( to.name === 'Login' && isValidSession())
    {
        next({path: '/homepage'});
    }
    /* 
        Si voy a una ruta que requiere autenticacion y no tengo una sesion valida
    */
    else if ( to.meta.authentication && !isValidSession())
    {
        next({path: '/login' });
    }else //cualquier otra ruta
    {
        next()
    }
})


export default router
