const defaultroutes = [
    {
      name:'LandingPage',
      path:'/',
      component: () => import(/* webpackChunkName: "landingpage" */ '../views/general/LandingPage.vue'),
      meta:{
        layout:'Default',
        authentication:false
      }
    },
    {
      name:'Login',
      path:'/login',
      component: () => import(/* webpackChunkName: "login" */ '../views/general/Login.vue'),
      meta:{
        layout:'Default',
        authentication:false
      }
    },
    {
      name:'About',
      path:'/about',
      component: () => import(/* webpackChunkName: "about" */ '../views/general/About.vue'),
      meta:{
        layout:'Default',
        authentication:false
      },
    },
    {
      name:'Register',
      path:'/register',
      component: () => import(/* webpackChunkName: "register" */ '../views/general/Register.vue'),
      meta:{
        layout:'Default',
        authentication:false
      }
    },
    {
      name:'ForgotPassword',
      path:'/forgotpassword',
      component: () => import(/* webpackChunkName: "forgotpassword" */ '../views/general/ForgotPassword.vue'),
      meta:{
        layout:'Default',
        authentication:false
      }
    }
]

export default defaultroutes