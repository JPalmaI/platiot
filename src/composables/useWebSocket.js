import { ref, onUnmounted, computed } from "vue";
import wsManager from "@/services/websocket.manager";

export default function useWebSocket() {
  // Estado de conexión
  const isConnected = ref(wsManager.isConnected);
  const isAuthenticated = ref(wsManager.isAuthenticated)

  // Actualizar estado cuando cambie la conexión
  const updateConnectionStatus = (connected) => {
    isConnected.value = connected;
  };

  const socketConnect = () => {
    if (!isConnected.value) {
      wsManager.connect();
      wsManager.shouldReconnect = true; //Solo se va a reconectar si se ejecuta desde la funcion socketConnect()
    }
  };

  const socketDisconnect = () => {
    if (isConnected.value) {
      wsManager.close();
      wsManager.shouldReconnect = false; //Solo se va a reconectar si se ejecuta desde esta funcion
    }
  };
  // Escuchar eventos de conexión del WebSocketManager
  wsManager.addConnectionListener(updateConnectionStatus);

  // Limpiar al desmontar el componente
  onUnmounted(() => {
    wsManager.removeConnectionListener(updateConnectionStatus);
  });

  // Registra listeners de mensajes con auto-remoción
  const onMessage = (messageType, callback) => {
    wsManager.addMessageListener(messageType, callback);

    // Auto-remover al desmontar
    onUnmounted(() => {
      wsManager.removeMessageListener(messageType, callback);
    });
  };

  return {
    isConnected: computed(() => isConnected.value),
    isAuthenticated: computed(()=>isAuthenticated.value),
    send: wsManager.sendMessage.bind(wsManager),
    onMessage,
    reconnectAttempts: computed(() => wsManager.reconnectAttempts),
    socketConnect,
    socketDisconnect,
  };
}
