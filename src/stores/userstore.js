import { defineStore } from "pinia";
import { ref } from "vue";
import { useJwt } from '@vueuse/integrations/useJwt'

export const useUserStore = defineStore('userstore',() => {

    const user = ref({
        name:null,
        last_name:null,
        email:null,
        token:null
    });

    function updateUserInfo(token)
    {
        if ('email' in token)
        {
            user.value.email = token.email;
        }

        if ( 'sub' in token)
        {
            user.value.name = token.sub;
        }

        if('lastname' in token)
        {
            user.value.last_name = token.lastname;
        }
        
        user.value.token = token
    }

    function login(userToken)
    {
        try {

            const { payload } = useJwt(userToken);

            updateUserInfo(payload.value);

        } catch (error) 
        {
            return false
        }

        localStorage.setItem('token',userToken);
        return true;
    }

    function logout(){
        user.value.email = null
        user.value.name = null
        user.value.last_name = null
        localStorage.removeItem('token')
    }

    function isValidSession(){

        const token = localStorage.getItem('token');

        if ( token === null )
        {
            return false
        }

        let expirationDate;
        let actualDate = new Date();
        let decoded_token;

        try {
            const { payload } = useJwt(token);

            if ('exp' in payload.value)
            {
                expirationDate = new Date(payload.value.exp * 1000)
            }

            decoded_token = payload.value; 

        } catch (error) {
            
            return false
        }

        if (actualDate > expirationDate)
        {
            return false
        }
        //Si la session es correcta actualizamos los valores de la store
        //para sobrevivir los actualizar de pagina(f5)
        updateUserInfo(decoded_token)
        return true
    }

    return { user, login, logout, isValidSession}
})
