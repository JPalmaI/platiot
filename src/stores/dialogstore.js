import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useDialogStore = defineStore('dialogstore', () => {
    const dialog = ref(false);
    const dialogMessage = ref('');
    const title = ref('')
    const color = ref('')

    function changeStatus(){
        dialog.value = !dialog.value;
    }

    function changeMessageToError(msg){
        title.value = 'Ha ocurrido un error';
        color.value = 'error'
        dialogMessage.value = msg
    }

    function changeMessageToSuccess(msg){
        title.value = 'Accion realizada correctamente';
        color.value = 'success'
        dialogMessage.value = msg

    }

    return { dialog, dialogMessage,title,color, changeStatus, changeMessageToError, changeMessageToSuccess }
})
