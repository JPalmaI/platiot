import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useWorkAreaStore = defineStore('workareastore', () => {
    
    const storeDashboard = ref({
        name:'',
        description:'',
        id:'',
        gridstack_layout:''
    });

    function storeCreateDashboard(data) 
    {
      storeDashboard.value.name = data.name;
      storeDashboard.value.description = data.description;
      storeDashboard.value.id = data.id;
      storeDashboard.value.gridstack_layout = data.gridstack_layout;

      return storeDashboard;
    }

    function storeResetDashboard()
    {
      storeDashboard.value.name = '';
      storeDashboard.value.description = '';
      storeDashboard.value.id = '';
      storeDashboard.value.gridstack_layout = '';
      return
    }

  return { storeDashboard, storeCreateDashboard, storeResetDashboard }
});
