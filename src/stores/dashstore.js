import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useDashDrawerStore = defineStore('dashdrawerstore', () => {
  const drawer = ref(true);

  function changeStatus(){
    drawer.value = !drawer.value
  }

  return { drawer, changeStatus }
})
