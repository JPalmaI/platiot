
export function registerLayouts(app){
    const layouts = import.meta.glob('./*.vue', { eager: true });
    for (const [key, value] of Object.entries(layouts)) {
        app.component(value.default.name,value.default);        
    }
};
