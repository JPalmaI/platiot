class WebSocketManager {
    static instance = null;
  
    constructor(url) {
      if (WebSocketManager.instance) {
        return WebSocketManager.instance;
      }
  
      // Estado interno mutable
      this._state = {
        socket: null,                   // Conexión WebSocket
        messageListeners: {},           // Callbacks por tipo de mensaje
        reconnectAttempts: 0,           // Intentos de reconexión actuales
        maxReconnectAttempts: 5,        // Límite de intentos
        reconnectInterval: 3000,        // Intervalo base (ms)
        isConnected: false,             // Estado de conexión
        connectionListeners: [],         // Callbacks para cambios de conexión
        authToken: null,                  //JWT para autenticacion
        shouldReconnect: false,           //Si deberia o no realizarse una reconexion
        isAuthenticated: false,            //Si esta o no autenticado con el ws
      };
  
      this.url = url;

      // Congelar la instancia (pero el state interno sigue siendo mutable)
      WebSocketManager.instance = Object.freeze(this);
    }
  
    connect() {
      this._state.socket = new WebSocket(this.url);
  
      this._state.socket.onopen = () => {
        console.log('WebSocket conectado');
        this._state.isConnected = true;
        this._state.reconnectAttempts = 0;
        this.sendLogin()
        //Notifica a todos los listeners cuando la conexion se ha realizado y
        //le pasa un true como parametro
        this._state.connectionListeners.forEach(cb => cb(true));
      };
  
      this._state.socket.onmessage = (event) => {
        try {
          const data = JSON.parse(event.data);
          this.handleMessage(data);
        } catch (error) {
          console.error('Error parsing message:', error);
        }
      };
  
      this._state.socket.onclose = () => {
        console.log('WebSocket desconectado');
        this._state.isConnected = false;
        this.handleReconnect();
        this._state.connectionListeners.forEach(cb => cb(false)); // Notificar
      };
  
      this._state.socket.onerror = (error) => {
        console.error('WebSocket error:', error);
      };
    }
  
    handleMessage(data) {
      if(data.type === "login"){
        if(data.authenticated == true){
          this._state.isAuthenticated = data.authenticated
          console.log(data.message)
        }
      }

      if (data.type && this._state.messageListeners[data.type]) {
        this._state.messageListeners[data.type].forEach(callback => {
          try {
            callback(data);
          } catch (error) {
            console.error('Error en message listener:', error);
          }
        });
      }
    }
  
    addMessageListener(messageType, callback) {
      if (!this._state.messageListeners[messageType]) {
        this._state.messageListeners[messageType] = [];
      }
      this._state.messageListeners[messageType].push(callback);
    }
  
    removeMessageListener(messageType, callback) {
      if (this._state.messageListeners[messageType]) {
        this._state.messageListeners[messageType] = 
          this._state.messageListeners[messageType].filter(cb => cb !== callback);
      }
    }
  
    sendMessage(type, payload = {}) {
      if (this._state.isConnected) {
        const message = JSON.stringify({ type, ...payload });
        this._state.socket.send(message);
      } else {
        console.warn('No se puede enviar mensaje - WebSocket desconectado');
      }
    }
  
    // Método para enviar el mensaje de login
    sendLogin() {
      this._state.authToken = localStorage.getItem("token")

      if (this._state.authToken && this._state.isConnected) {
        try {
          const message = JSON.stringify({
            type: 'login',
            payload: this._state.authToken
          });
          this._state.socket.send(message);
          console.log('Login enviado');
        } catch (error) {
          console.error('Error enviando login:', error);
        }
      }
    }

    handleReconnect() {
      if (this._state.reconnectAttempts < this._state.maxReconnectAttempts && this._state.shouldReconnect) {
        setTimeout(() => {
          this._state.reconnectAttempts++;
          console.log(`Reconexión intento #${this._state.reconnectAttempts}`);
          this.connect();
        }, this._state.reconnectInterval * Math.pow(2, this._state.reconnectAttempts));
      }
    }
  
    close() {
      if (this._state.socket) {
        this._state.socket.close();
        this._state.reconnectAttempts = 0;
        this._state.isConnected = false;
        WebSocketManager.instance = null;
      }
    }
    
    addConnectionListener(callback) {
        this._state.connectionListeners.push(callback);
    }
    
    removeConnectionListener(callback) {
        this._state.connectionListeners = 
        this._state.connectionListeners.filter(cb => cb !== callback);
    }
    

    // Getters públicos
    get isConnected() {
      return this._state.isConnected;
    }
  
    get reconnectAttempts() {
      return this._state.reconnectAttempts;
    }

    set shouldReconnect(value){
      if(typeof value === 'boolean'){
        this._state.shouldReconnect = value;
      }else{
        console.error('Valor inválido para shouldReconnect');
      }
    }

    get isAuthenticated(){
      return this._state.isAuthenticated;
    }

  }
  
  // Configuración y exportación
  const wsInstance = new WebSocketManager(import.meta.env.VITE_WS_URL);
  export default wsInstance;