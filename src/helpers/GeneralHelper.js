import { useDialogStore } from '@/stores/dialogstore';
import { storeToRefs } from 'pinia';

const store = useDialogStore()
const { dialog } = storeToRefs(store)
const { changeStatus, changeMessageToError, changeMessageToSuccess } = store

export function openErrorDialog(message){
    if (dialog.value === true)
        dialog.value = false
    
    changeMessageToError(message)
    changeStatus()
}

export function openSuccessDialog(message){
    if (dialog.value === true)
        dialog.value = false
    
    changeMessageToSuccess(message)
    changeStatus()
}
