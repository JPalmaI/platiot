import apiconfig from '@/api/config'
import { fetchWrapper } from '@/api/fetchWrapper';

const DEVICES_API_URL = apiconfig.API_URL + '/devices';

export async function fetchAllDevices(){

    const response = await fetchWrapper.get(DEVICES_API_URL + '/all');

    return response;
}
