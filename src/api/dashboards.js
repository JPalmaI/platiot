import apiconfig from '@/api/config'
import { fetchWrapper } from '@/api/fetchWrapper';

const DASHBOARDS_API_URL = apiconfig.API_URL + '/dashboards';

export async function fetchDashboards(){

    const response = await fetchWrapper.get(DASHBOARDS_API_URL + '/all');

    return response;
}

export async function fetchDeleteDashboard(dashboardId){
    const response = await fetchWrapper.post(DASHBOARDS_API_URL + '/delete',dashboardId);

    return response;
}

export async function fetchUpdateDashboard(dashboard){
    const response = await fetchWrapper.post(DASHBOARDS_API_URL + '/update',dashboard);

    return response;
}

export async function fetchNewDashboard(newDashboard){
    const response = await fetchWrapper.post(DASHBOARDS_API_URL + '/new',newDashboard);

    return response;
}