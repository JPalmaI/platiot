import apiconfig from '@/api/config'
import { fetchWrapper } from './fetchWrapper';

/**
 * @param {Object} userData - The dictionary containing user data
 */
export async function fetchCreateUser(userData = null) {
    
    if( userData === null )
    {
        throw new Error('No user provided');
    }

    const response = await fetchWrapper.post(apiconfig.API_URL + '/users/', userData );

    return response;
}

/**
 * @param {Object} userData - The dictionary containing user data
 */
export async function fetchCreateSession(userData = null) {

    if (userData === null || !('email' in userData) || !('password' in userData))
    {
        throw new Error('No user provided')
    }

    const user = new FormData();
    user.append('username',userData['email'])
    user.append('password',userData['password'])

    const response = await fetchWrapper.login(apiconfig.API_URL + '/token', user);

    return response;
}
