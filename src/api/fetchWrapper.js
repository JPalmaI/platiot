/* 
    Fetch Wrapper by Jason Watmore's Blog
*/
export const fetchWrapper = {
    get,
    post,
    put,
    delete: _delete,
    login
};

function get(url) {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('token')}` ,
            'Content-Type': "application/json",
        }
    };
    return fetch(url, requestOptions).then(handleResponse);
}

function post(url, body) {
    const requestOptions = {
        method: 'POST',
        headers: { 
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`  ,
        },
        body: JSON.stringify(body)
    };
    return fetch(url, requestOptions).then(handleResponse);
}

function login(url,body){
    const requestOptions = {
        method: 'POST',
        body: body
    };
    return fetch(url, requestOptions).then(handleResponse);
}

function put(url, body) {
    const requestOptions = {
        method: 'PUT',
        headers: { 
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`  ,
        },
        body: JSON.stringify(body)
    };
    return fetch(url, requestOptions).then(handleResponse);    
}

// prefixed with underscored because delete is a reserved word in javascript
function _delete(url) {
    const requestOptions = {
        method: 'DELETE',
        headers: { 
            'Authorization': `Bearer ${localStorage.getItem('token')}`  ,
        },
    };
    return fetch(url, requestOptions).then(handleResponse);
}

// helper functions

function handleResponse(response) {

    return response.text().then(text => {
        const data = text && JSON.parse(text);

        if (!response.ok) { //Si la respuesta es negativa
            //Si existe data y tiene una propiedad .detail y es un arreglo
            //unimos los mensajes de error, de lo contrario enviamos el statusText de la respuesta
            const error = (data && data.detail && Array.isArray(data.detail)) //Error de validación Pydantic/FastAPI []
            ? data.detail.map(item => item.msg) // Extrae y une los mensajes
            : response.statusText;

            if(!Array.isArray(error))
                return Promise.reject(data.detail)
            
            return Promise.reject(error);
        }

        return data;
    });
}

