const apiconfig = Object.freeze({
    API_URL: import.meta.env.VITE_API_URL,
  })

export default apiconfig