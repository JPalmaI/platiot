import apiconfig from '@/api/config'
import { fetchWrapper } from '@/api/fetchWrapper';

const DEVICES_API_URL = apiconfig.API_URL + '/devices';

export async function fetchDevicesList(){

    const response = await fetchWrapper.get(DEVICES_API_URL + '/all');

    return response;
}

export async function fetchDeleteDevice(deviceId){
    const response = await fetchWrapper.post(DEVICES_API_URL + '/delete',deviceId);

    return response;
}

export async function fetchUpdateDevice(device){
    const response = await fetchWrapper.post(DEVICES_API_URL + '/update',device);

    return response;
}

export async function fetchNewDevice(newDevice){
    const response = await fetchWrapper.post(DEVICES_API_URL + '/new',newDevice);

    return response;
}