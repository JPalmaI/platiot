import './assets/main.css'
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader
//import { createApp } from 'vue'
import { createApp } from 'vue/dist/vue.esm-bundler';
//https://github.com/fengyuanchen/vue-feather/issues/8
import App from './App.vue'

//Libreria para graficos apexcharts
import VueApexCharts from "vue3-apexcharts";

// pinia (store)
import { createPinia } from 'pinia'

//vue-router
import router from './router'

//vue-query
import { VueQueryPlugin, QueryClient } from "@tanstack/vue-query";

// Vuetify
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

//Layouts
import { registerLayouts } from './views/layouts/register';

const vuetify = createVuetify({
    icons: {
        defaultSet: 'mdi', // This is already the default value - only for display purposes
    },
    components,
    directives,
})


const app = createApp(App)
app.use(createPinia())
app.use(VueQueryPlugin)
app.use(router)
app.use(vuetify)
registerLayouts(app)
app.use(VueApexCharts);
app.mount('#app')
