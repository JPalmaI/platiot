import { fileURLToPath, URL } from 'node:url'
import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig(({ mode }) =>{

  const env = loadEnv(mode, process.cwd());

  const SERVER_HOST = `${env.VITE_SERVER_HOST ?? 'localhost'}`;
  const SERVER_PORT = `${env.VITE_SERVER_PORT ?? '3000'}`;

  return { 
    plugins: [
      vue({
        template: {
          compilerOptions: {
            //isCustomElement: (tag) => ['LandingPage'].includes(tag),
          }
        }
      }),
    ],
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url))
      }
    },
    server: {
      host: SERVER_HOST,
      port: SERVER_PORT
    }
  }
})
